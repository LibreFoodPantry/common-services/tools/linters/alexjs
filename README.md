# AlexJS

[AlexJS](https://alexjs.com/) helps you find gender favouring, polarising,
race related, religion inconsiderate, or other unequal phrasing.

## Using in the Pipeline

To enable alexjs in your project's pipeline add `alexjs` to the `ENABLE_JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "alexjs"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the
[AlexJS Linter extension](https://marketplace.visualstudio.com/items?itemName=TLahmann.alex-linter)
for Visual Studio Code.

## Using Locally in Your Development Environment

AlexJS can be run locally in a linux-based bash shell or in
a linting shell script with this Docker command:

```bash
docker run -v "${PWD}":/workdir -w /workdir
    registry.gitlab.com/librefoodpantry/common-services/tools/linters/alexjs:latest
    alex .
```

## Configuration

Each project that uses the alexjs tool may have a `.alexignore` file which
is used for eslint configuration.

This project contains an example `.alexignore` file that will have AlexJS
ignore full files. See
[Ignoring Files](https://github.com/get-alex/alex#ignoring-files).

You may also use HTML comments to have AlexJS ignore lines in a file. See
[Control](https://github.com/get-alex/alex#control).

This file is used by the pipeline tool, the Visual Studio Code extension,
and the locally run Docker image.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
