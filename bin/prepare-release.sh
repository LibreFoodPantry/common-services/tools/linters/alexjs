#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/alexjs.yml
cat ./source/gitlab-ci/alexjs.yml >> ./artifacts/release/alexjs.yml
